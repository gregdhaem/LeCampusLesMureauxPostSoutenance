<?php

namespace App\Controller;

use App\Entity\TEvents;
use App\Entity\TArticles; // Appel de la classe entity Tarticles (base de données)
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\HttpFoundation\Response;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Symfony\Component\Routing\Annotation\Route; // Routage
use Symfony\Component\Form\Extension\Core\Type\DateType; // Formulaires

use Symfony\Component\Form\Extension\Core\Type\TextType; // Formulaires
use Symfony\Component\Form\Extension\Core\Type\SubmitType; // Formulaires
use Symfony\Component\Form\Extension\Core\Type\TextareaType; // Formulaires
use Symfony\Component\Form\Extension\Core\Type\FileType;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method; // Méthode Get et Push

class AdminController extends Controller
{
    /**
     * @Route("/admin/manage", name="admin")
     */
    public function index()
    {
        return $this->render('admin/index.html.twig', [
            'controller_name' => 'AdminController',
        ]);
    }

    /**
     * @Route("/admin/article/list", name="article_list")
     * @Method({"GET"})
     */
    public function listArticles() 
    {

        $articles= $this->getDoctrine()->getRepository(TArticles::class)->findAll();
  
        return $this->render('admin/articlelist.html.twig', array('articles' => $articles));
    }
    
    /**
     * @Route("/admin/article/delete/{id}")
     * @Method({"DELETE"})
     */
    public function deleteArticle(Request $request, $id) 
    {
        $article = $this->getDoctrine()->getRepository(TArticles::class)->find($id);
  
        $em = $this->getDoctrine()->getManager();
        $em->remove($article);
        $em->flush();
  
        $response = new Response('Suppression de l\'article n° '. $article->getId());
        $response->send();

        return $this->redirectToRoute('article_list');
    }
    
    /**
     * @Route("/admin/article/modifier/{id}", name="edit_article")
     * @Method({"GET", "POST"})
     */
    public function editArticle(Request $request, $id) 
    {
        $article = new TArticles();
        $article = $this->getDoctrine()->getRepository(TArticles::class)->find($id);
  
        $form = $this->createFormBuilder($article)
            ->add('title', TextType::class, array(
                'attr' => array(
                        'class' => 'form-control')
            ))
            ->add('author', TextType::class, array(
                'required' => false,
                'attr' => array(
                        'class' => 'form-control')
            ))
            ->add('date', DateType::class)
            
            ->add('image', TextType::class, array(
                'required' => false,
                'attr' => array(
                        'class' => 'form-control')
            ))
            ->add('content', TextareaType::class, array(
                'required' => false,
                'attr' => array(
                        'class' => 'form-control')
            ))  
            ->add('save', SubmitType::class, array(
                'label' => 'Modifier l\'article',
                'attr' => array('class' => 'btn btn-link btn-block mt-3')
            ))
            ->getForm();
  
        $form->handleRequest($request);
  
        if($form->isSubmitted() && $form->isValid()) {
  
            $em = $this->getDoctrine()->getManager();
            $em->flush();
  
            return $this->redirectToRoute('article_show', ['id' => $article->getId()]);
        }
  
        return $this->render('admin/editarticle.html.twig', array(
            'form' => $form->createView()
        ));
    }
   
      /**
     * @Route("/admin/article/new", name="new_article")
     * @Method({"GET", "POST"})
     */
    public function newArticle(Request $request) 
    {
        $article = new TArticles();
  
        $form = $this->createFormBuilder($article)
            ->add('title', TextType::class, array(
                'attr' => array(
                        'class' => 'form-control',
                        'placeholder' => 'Titre de l\'article...')
            ))
            ->add('author', TextType::class, array(
                'required' => false,
                'attr' => array(
                        'class' => 'form-control',
                        'placeholder' => 'Auteur de l\'article...')
            ))
            ->add('date', DateType::class, array(
                'years' => array('2018', '2019', '2020')
            ))

            ->add('image', FileType::class, array(
                'required' => false,
                'attr' => array(
                        'class' => 'form-control')
            ))
            ->add('content', TextareaType::class, array(
                'required' => false,
                'attr' => array(
                        'class' => 'form-control',
                        'placeholder' => 'Contenu de l\'article...')
            ))  
            ->add('save', SubmitType::class, array(
                'label' => 'Enregistrer l\'article',
                'attr' => array(
                        'class' => 'btn btn-link btn-block mt-3')
            ))
            ->getForm();
  
        $form->handleRequest($request);
  
        if($form->isSubmitted() && $form->isValid()) 
        {
            $file = $article->getImage();
            $fileName = md5(uniqid()) . '.' . $file->guessExtension();
            $file->move($this->getParameter('upload_directory'), $fileName);
            $article->setImage($fileName);

            $article = $form->getData();
  
            $em = $this->getDoctrine()->getManager();
            $em->persist($article);
            $em->flush();
  
            return $this->redirectToRoute('article_show', ['id' => $article->getId()]);
        } 
        return $this->render('admin/newarticle.html.twig', array('form' => $form->createView()
        ));
    }

    /**
     * @Route("/admin/article/{id}", name="article_show")
     * @Method({"GET"})
     */
    public function showArticle($id) 
    {
        $article = $this->getDoctrine()->getRepository(TArticles::class)->find($id);
  
        return $this->render('admin/showsinglearticle.html.twig', array('article' => $article));
    }

    /**
     * @Route("/admin/evenement/list", name="event_list")
     * @Method({"GET"})
     */
    public function listEvents() 
    {
        $events= $this->getDoctrine()->getRepository(TEvents::class)->findAll();
  
        return $this->render('admin/eventlist.html.twig', array('events' => $events));
    }

    /**
     * @Route("/admin/evenement/modifier/{id}", name="edit_event")
     * @Method({"GET", "POST"})
     */
    public function editEvent(Request $request, $id)
    {
        $event = new TEvents();
        $event = $this->getDoctrine()->getRepository(TEvents::class)->find($id);
  
        $form = $this->createFormBuilder($event)
            ->add('title', TextType::class, array(
                'attr' => array(
                        'class' => 'form-control')
            ))
            ->add('image', TextType::class, array(
                'attr' => array(
                        'class' => 'form-control')
            ))
            ->add('begin_date', DateType::class)

            ->add('end_date', DateType::class, array(
                'help' => 'Pour enlever un événement du site il suffit d\'entrer une date postérieure à la date du jour'
            ))  

            ->add('save', SubmitType::class, array(
                'label' => 'Modifier l\'événement',
                'attr' => array('class' => 'btn btn-link btn-block mt-3')
            ))
            ->getForm();
  
        $form->handleRequest($request);
  
        if($form->isSubmitted() && $form->isValid()) {
  
            $em = $this->getDoctrine()->getManager();
            $em->persist($event);
            $em->flush();
  
            return $this->redirectToRoute('event_show', ['id' => $event->getId()]);
        }  
        return $this->render('admin/editevent.html.twig', array('form' => $form->createView()
        ));
    }

    /**
     * @Route("/admin/evenement/new", name="new_event")
     * @Method({"GET", "POST"})
     */
    public function newEvent(Request $request)
    {
        $event = new TEvents();
  
        $form = $this->createFormBuilder($event)
            ->add('title', TextType::class, array(
                'attr' => array(
                        'class' => 'form-control')
            ))
            ->add('image', FileType::class, array(
                'attr' => array(
                        'class' => 'form-control-file')
            ))
            ->add('begin_date', DateType::class, array(
                'years' => array('2018', '2019', '2020')
            ))
              
            ->add('end_date', DateType::class, array(
                'years' => array('2018', '2019', '2020')
            ))

            ->add('save', SubmitType::class, array(
                'label' => 'Enregistrer l\'événement',
                'attr' => array('class' => 'btn btn-link btn-block mt-3')
            ))
            ->getForm();
  
        $form->handleRequest($request);
  
        if($form->isSubmitted() && $form->isValid()) {

            $file = $event->getImage();
            $fileName = md5(uniqid()) . '.' . $file->guessExtension();
            $file->move($this->getParameter('upload_directory'), $fileName);
            $event->setImage($fileName);
  
            $em = $this->getDoctrine()->getManager();
            $em->persist($event);
            $em->flush();

            return $this->redirectToRoute('event_show', ['id' => $event->getId()]);
        }  
        return $this->render('admin/newevent.html.twig', array('form' => $form->createView()
        ));
    }

    /**
     * @Route("/admin/evenement/{id}", name="event_show")
     * @Method({"GET"})
     */
    public function showEvent($id)
    {
        $event = $this->getDoctrine()->getRepository(TEvents::class)->find($id);
  
        return $this->render('admin/showsingleevent.html.twig', array('event' => $event));
    }    

    /**
     * @Route("/admin/evenement/delete/{id}")
     * @Method({"DELETE"})
     */
    public function deleteEvent(Request $request, $id)
    {
        $event = $this->getDoctrine()->getRepository(TEvents::class)->find($id);
  
        $em = $this->getDoctrine()->getManager();
        $em->remove($event);
        $em->flush();
  
        $response = new Response('Suppression de l\'événement n° '. $event->getId());
        $response->send();

        return $this->redirectToRoute('event_list');
    }    



}
