<?php
namespace App\Controller;
use App\Entity\TUsers;
use App\Repository\TUsersRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method; // Méthode Get et Push
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
class FormController extends Controller
{
     /**
     * @Route("/inscription", name="users")
     * @Method({"GET", "POST"})
     */
    public function form(TUsers $tusers = null, Request $request, ObjectManager $manager)
    {
        if(!$tusers){
            $tusers = new TUsers();
        }
        $form = $this->createFormBuilder($tusers)
            ->add('sexe', ChoiceType::class, [
                'choices'  => array(
                    'Civilité' => NULL,
                    '--------' => NULL,
                    'Madame' => 'Madame',
                    'Monsieur' => 'Monsieur'
                ),
                'label' => false
                ])
            ->add('first_name', TextType::class, ['label' => false])
            ->add('last_name', TextType::class, ['label' => false])
            ->add('email', EmailType::class, ['label' => false])
            ->add('password', PasswordType::class, ['label' => false])
            ->add('direct_phone', TextType::class, array('label' =>false,'required' => false))
            ->add('cell_phone', TextType::class, array('label' =>false,'required' => false))
            ->add('company', TextType::class, array('label' =>false,'required' => false))
            ->add('job_title', ChoiceType::class, array(
                'choices'  => array(
                    'Fonction / Service' => NULL,
                    'PDG-DG-Gérant' => 'PDG-DG-Gérant',
                    'Dir/Resp Commercial' => 'Dir/Resp Commercial',
                    'Dir/Resp des Achats' => 'Dir/Resp des Achats',
                    'Dir/Resp Export' => 'Dir/Resp Export',
                    'Dir/Resp Finance-Compta-Gestion' => 'Dir/Resp Finance-Compta-Gestion',
                    'Dir/Resp Formation' => 'Dir/Resp Formation',
                    'Dir/Resp Informatique' => 'Dir/Resp Informatique',
                    'Dir/Resp Juridique' => 'Dir/Resp Juridique',
                    'Dir/Resp Logistique' => 'Dir/Resp Logistique',
                    'Dir/Resp Marketing-Communication' => 'Dir/Resp Marketing-Communication',
                    'Dir/Resp Ressources Humaines' => 'Dir/Resp Ressources Humaines',
                    'Dir/Resp Securite' => 'Dir/Resp Securite',
                    'Dir/Resp Services Generaux' => 'Dir/Resp Services Generaux',
                    'Dir/Resp Technique - Production' => 'Dir/Resp Technique - Production',
                    'Service Achats' => 'Service Achats',
                    'Service Commercial' => 'Service Commercial',
                    'Service Direction Generale' => 'Service Direction Generale',
                    'Service Export' => 'Service Export',
                    'Service Finance-Compta-Gestion' => 'Service Finance-Compta-Gestion',
                    'Services Generaux' => 'Services Generaux',
                    'Service Informatique' => 'Service Informatique',
                    'Service Juridique' => 'Service Juridique',
                    'Service Logistique' => 'Service Logistique',
                    'Service Marketing-Communication' => 'Service Marketing-Communication',
                    'Service Ressources Humaines' => 'Service Ressources Humaines',
                    'Service Technique-Production' => 'Service Technique-Production',
                    'Architecte - Bureau Etude' => 'Architecte - Bureau Etude',
                    'Prof. Juridique et reglementee' => 'Prof. Juridique et reglementee',
                    'Prof. Lib-Consultant' => 'Prof. Lib-Consultant',
                    'Elu/Maire' => 'Elu/Maire',
                    'Comite Entreprise' => 'Comite Entreprise',
                    'Autres' => 'Autres',
                ), 'label' => false))
            ->add('badge', CheckboxType::class, ['required' => false])
            ->add('newsletter', CheckboxType::class, ['required' => false])
            ->add('doc_request', CheckboxType::class, ['required' => false])           
            ->getForm();
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            $manager = $this->getDoctrine()->getManager();
            $manager->persist($tusers);
            $manager->flush();
            dump($tusers);
            return $this->redirectToRoute('confirm', [
                'id' => $tusers->getId(),
                'users' => $tusers
            ]);
        }
        return $this->render('form/users.html.twig', [
            'formTUsers' => $form->createView(),
        ]);
    }

    /**
     * @Route("/inscription/confirm", name="confirm")
     */
    public function confirm(Tusers $users, Request $request, ObjectManager $manager)
    {
        $users = new TUsers();
        $users= $this->getDoctrine()->getRepository(TUsers::class)->find('id');
        return $this->render('form/confirm.html.twig', [
            'controller_name' => 'confirm',
            ['users' => $users]
        ]);
    }
}