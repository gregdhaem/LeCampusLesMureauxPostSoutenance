<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\TArticles;
use App\Entity\TEvents;
use App\Entity\TUsers;
use Faker\Factory;

class LeCampusFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $faker = \Faker\Factory::create('fr_FR');

        $content = '<p>';
        $content .= join($faker->paragraphs(3), '</p><P>');
        $content .= '</p>';

        for($i = 1; $i < 12 ; $i++)
        {
            $tarticles = new TArticles();
            $tarticles->setTitle($faker->sentence())
                    ->setAuthor("Auteur n° $i")
                    ->setDate($faker->dateTimeBetween('-6 months'))
                    ->setImage($faker->imageUrl())
                    ->setContent($content);
            $manager->persist($tarticles);
        }

        for($i = 1; $i < 9; $i++)
        {
            $events = new TEvents();
            $events->setTitle("Titre n° $i")
                    ->setImage($faker->imageUrl())
                    ->setBeginDate($faker->dateTimeBetween('-2 months'))
                    ->setEndDate($faker->dateTimeBetween('-1 months'));
            $manager->persist($events);
        }

        for($i = 1; $i < 70; $i++)
        {
            $users = new TUsers();
            $users->setNewsletter("yes")
                    ->setDocRequest("no")
                    ->setSexe("male")
                    ->setFirstName($faker->firstname())
                    ->setLastName($faker->lastname())
                    ->setEmail($faker->email())
                    ->setCompany($faker->company())
                    ->setDirectPhone($faker->phoneNumber())
                    ->setCellPhone($faker->phonenumber())
                    ->setJobTitle($faker->jobtitle())
                    ->setBadge("http://placehold.it/150x150")
                    ->setPassword("123456");
            $manager->persist($users);
        }

        $manager->flush();
    }
}
