<?php

namespace App\Repository;

use App\Entity\TUsers;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method TUsers|null find($id, $lockMode = null, $lockVersion = null)
 * @method TUsers|null findOneBy(array $criteria, array $orderBy = null)
 * @method TUsers[]    findAll()
 * @method TUsers[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TUsersRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, TUsers::class);
    }

//    /**
//     * @return TUsers[] Returns an array of TUsers objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TUsers
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
