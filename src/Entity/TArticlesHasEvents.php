<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TArticlesHasEventsRepository")
 */
class TArticlesHasEvents
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\TArticles")
     * @ORM\JoinColumn(nullable=false)
     */
    private $tarticles_id_articles;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\TEvents")
     * @ORM\JoinColumn(nullable=false)
     */
    private $tevents_id_events;

    public function getId()
    {
        return $this->id;
    }

    public function getTarticlesIdArticles(): ?TArticles
    {
        return $this->tarticles_id_articles;
    }

    public function setTarticlesIdArticles(?TArticles $tarticles_id_articles): self
    {
        $this->tarticles_id_articles = $tarticles_id_articles;

        return $this;
    }

    public function getTeventsIdEvents(): ?TEvents
    {
        return $this->tevents_id_events;
    }

    public function setTeventsIdEvents(?TEvents $tevents_id_events): self
    {
        $this->tevents_id_events = $tevents_id_events;

        return $this;
    }
}
